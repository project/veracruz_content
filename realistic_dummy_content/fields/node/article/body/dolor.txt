Sobre PRECIADO SERRANO

<strong>Juan José González Fernández.</strong>

<em>Este es el relato de la vida de un valiente hijo de la ciudad de Veracruz, para quien las generaciones de coterráneos que le siguieron en la vida, no han tenido aún la distinción de recordar su nombre.</em>

<strong>Preciado Serrano era hijo de distinguida y rica familia veracruzana.</em>

<em>Abrazó la carrera de las armas a fines dé la dominación española en México, y al iniciarse el movimiento Insurgente puso su espada al servicio de la causa de la Independencia en el Batallón de Tres Villas.</em>

En todo el territorio Nacional dominaban las fuerzas Insurgentes y era jefe militar de Veracruz don Antonio López de Santa Anna, que no veía con buenos ojos ondear el pabellón español en la fortaleza de San Juan de Ulúa dominada aún por e} General Lemaur. Santa Anna, el hombre de los mil recursos, ideó una estratagema para posesionarse del Islote fortificado y entró en tratos con Lemaur haciéndole creer que estaba dispuesto a entregar la Plaza, indicando que cierta noche prefijada podría enviar sus tropas para que se posesionaran de los baluartes de la Concepción y Santiago, situados a los extremos de la muralla que daba al mar.
