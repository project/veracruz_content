<strong>EL ASALTO PIRATA DE 1683.</strong>

<em>Aquella memorable mañana del diecisiete de mayo de 1683, la Nueva Veracruz amaneció con la esperanza de ver llegar las naves de la Flota, que por esa época del año solía arribar conduciendo nuevos colonos y todas las mercancías que aún no producían las tierras conquistadas.</em>

<em>Desde temprana hora los madrugadores subieron a los torreones que coronaban las casas del Puerto, con la esperanza de ser los primeros en dar la noticia del feliz arribo, y en el Barrio de La Caleta, habitado por un centenar de pescadores se hacían comentarios criticando el que por cosas baladíes no hubieran salido de pesca en ese día.</em>

Al disiparse la niebla matutina empezó a brillar el sol en todo su esplendor, obligando a los curiosos que habían acudido a la playa o que se encontraban en los torreones, a buscar refugio bajo techo y la ciudad volvió a cobrar nuevamente su vida habitual.

