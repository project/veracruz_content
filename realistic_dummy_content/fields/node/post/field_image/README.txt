The files herein are used by Realistic dummy content to replace generated content for image fields in article nodes (entity type: node; bundle: article; this is determined by the directory structure -- node/article/field_image).

Some files might contain special characters on purpose so our test can make sure they are still imported correctly.

I, Koffermejia take this photos so is in my right to crop, Edit and share it.

You can contact me or see more photos like this in 
https://commons.wikimedia.org/wiki/User:Koffermejia


Alt Text
--------

Note that one of the photos has alt text, this is achieved using the attribute mechanism described in the documentation at ./README.md.
